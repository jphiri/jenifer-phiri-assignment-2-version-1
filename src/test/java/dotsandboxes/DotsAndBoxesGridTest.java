package dotsandboxes;

import org.junit.jupiter.api.*;
import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assumptions.*;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
//code adapted from Chatgpt
public class DotsAndBoxesGridTest {
    /*
     * Because Test classes are classes, they can have fields, and can have static fields.
     * This field is a logger. Loggers are like a more advanced println, for writing messages out to the console or a log file.
     */
    private static final Logger logger = LogManager.getLogger(DotsAndBoxesGridTest.class);

    /*
     * Tests are functions that have an @Test annotation before them.
     * The typical format of a test is that it contains some code that does something, and then one
     * or more assertions to check that a condition holds.
     *
     * This is a dummy test just to show that the test suite itself runs
     */
    @Test
    public void testTestSuiteRuns() {
        logger.info("Dummy test to show the test suite runs");
        assertTrue(true);
    }

    // FIXME: You need to write tests for the two known bugs in the code.

        @Test
        void testBoxCompleteOnEdge() {

            DotsAndBoxesGrid grid = new DotsAndBoxesGrid(4, 3, 2); // 4x3 grid with 2 players


            assertFalse(grid.boxComplete(3, 2)); // The bottom-right corner, there's no box here
        }

        @Test
        void testBoxCompleteOnEmptyBox() {

            DotsAndBoxesGrid grid = new DotsAndBoxesGrid(4, 3, 2); // 4x3 grid with 2 players


            assertFalse(grid.boxComplete(1, 1));
        }

        @Test
        void testBoxCompleteOnCompletedBox() {

            DotsAndBoxesGrid grid = new DotsAndBoxesGrid(4, 3, 2); // 4x3 grid with 2 players

            // Draw all lines to complete a box at coordinates (1, 1)
            grid.drawHorizontal(0, 1, 1);
            grid.drawHorizontal(0, 2, 1);
            grid.drawVertical(0, 1, 1);
            grid.drawVertical(1, 1, 1);

            // Test when the given coordinates represent a completed box
            assertTrue(grid.boxComplete(1, 1));
        }



    //end here fix me

    //fix 2


        @Test
        public void testDrawHorizontalOutOfBounds() {
            int width = 4;
            int height = 3;
            int players = 2;
            DotsAndBoxesGrid grid = new DotsAndBoxesGrid(width, height, players);

            // Test the out of bounds scenarios
            assertThrows(IndexOutOfBoundsException.class, () -> grid.drawHorizontal(-1, 0, 1));
            assertThrows(IndexOutOfBoundsException.class, () -> grid.drawHorizontal(width - 1, 0, 1));
            assertThrows(IndexOutOfBoundsException.class, () -> grid.drawHorizontal(0, -1, 1));
            assertThrows(IndexOutOfBoundsException.class, () -> grid.drawHorizontal(0, height, 1));
        }


    //end here
}
